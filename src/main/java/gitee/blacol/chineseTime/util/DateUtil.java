package gitee.blacol.chineseTime.util;

import java.util.Date;

/**
 * <p>DateUtil class.</p>
 *
 * @author Blacol
 */
public class DateUtil {
    /**
     * <p>getWesternDate.</p>
     * 将Date对象按照年月日拆分成int数组
     * @param date 要拆分的日期
     * @return 拆分结果
     */
    public static int[] getWesternDate(Date date){
        int year = date.getYear() + 1900;
        int month=date.getMonth()+1;
        int day=date.getDate();
        int[] result={year,month,day};
        return result;
    }
    /**
     * <p>getWesternTime.</p>
     * 将Date对象按照时分秒拆分成int数组
     * @param date 要拆分的日期
     * @return 拆分的结果
     */
    public static int[] getWesternTime(Date date){
        int hour = date.getHours();
        int minute=date.getMinutes();
        int second=date.getSeconds();
        int[] result={hour,minute,second};
        return result;
    }
}

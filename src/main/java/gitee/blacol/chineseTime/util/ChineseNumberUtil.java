package gitee.blacol.chineseTime.util;

import gitee.blacol.chineseTime.entity.ChineseConsts;

/**
 * <p>ChineseNumberUtil class.</p>
 * 阿拉伯数字转中文数字类
 * @author Blacol
 */
public class ChineseNumberUtil {
    /**
     * <p>numberToChinese.</p>
     *
     * @param number a int
     * @return a {@link java.lang.String} object
     */
    public static String numberToChinese(int number){
        StringBuffer stringBuffer=new StringBuffer();
        if (number<=10){
            return stringBuffer.append(ChineseConsts.数字[number]).toString();
        }else {
            int shi=number/10;
            int ge=number%10;
            if (shi==1&&ge==0){
                return stringBuffer.append(ChineseConsts.数字[10]).toString();
            }else if (shi==1){
                return stringBuffer.append(ChineseConsts.数字[10]).append(ChineseConsts.数字[ge]).toString();
            }else if(ge!=0){
                return stringBuffer.append(ChineseConsts.数字[shi]).append(ChineseConsts.数字[10]).append(ChineseConsts.数字[ge]).toString();
            }else{
                return stringBuffer.append(ChineseConsts.数字[shi]).append(ChineseConsts.数字[10]).toString();
            }
        }
    }
}

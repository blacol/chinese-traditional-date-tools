package gitee.blacol.chineseTime.entity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>ChineseConsts class.</p>
 * 中文常量类
 * @author Blacol
 */
public class ChineseConsts {
    /**
     * 天干数组
     */
    public static char[] 天干={'癸','甲','乙','丙','丁','戊','己','庚','辛','壬'};
    /**
     * 地支数组
     */
    public static char[] 地支={'亥','子','丑','寅','卯','辰','巳','午','未','申','酉','戌'};
    /**
     * 汉字数字
     */
    public static char[] 数字={'零','一','二','三','四','五','六','七','八','九','十'};
    /**
     * 时辰符号数组
     */
    public static char[] 时辰符号={'末','初'};

    /**
     * 获取节气表，以判断是否开始新的一个月
     *
     * @return 标志进入下一个月份的节气日期表
     */
    public static Map<String,Integer[]> 取节气表(){
        Map<String,Integer[]> 节气表=new LinkedHashMap<>();
        节气表.put("小寒",new Integer[]{1,5});
        节气表.put("立春",new Integer[]{2,4});
        节气表.put("惊蛰",new Integer[]{3,6});
        节气表.put("清明",new Integer[]{4,5});
        节气表.put("立夏",new Integer[]{5,6});
        节气表.put("芒种",new Integer[]{6,5});
        节气表.put("小暑",new Integer[]{7,7});
        节气表.put("立秋",new Integer[]{8,8});
        节气表.put("白露",new Integer[]{9,7});
        节气表.put("寒露",new Integer[]{10,7});
        节气表.put("立冬",new Integer[]{11,7});
        节气表.put("大雪",new Integer[]{12,7});
        return 节气表;
    }

}

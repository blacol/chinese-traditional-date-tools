package gitee.blacol.chineseTime.entity;

/**
 * <p>TransferMode class.</p>
 * 转换模式
 * @author Blacol
 */
public enum TransferMode {
    日期, 传统日期时间, 指定格式, 传统日期时间_截止到时辰, 完整的日期时间_简记
}

package gitee.blacol.chineseTime.entity;

/**
 * <p>ChineseTime class.</p>
 * 干支时间类
 * @author Blacol
 */
public class ChineseTime {
    private char 年干;
    private char 年支;
    private char 月干;
    private char 月支;
    private char 日干;
    private char 日支;
    private char 时天干;
    private char 时地支;
    private char 时辰符号;
    private String 分钟;
    private String 秒;
    private TransferMode 模式;

    /**
     * <p>取年干.</p>
     *
     * @return a char
     */
    public char 取年干() {
        return 年干;
    }

    /**
     * <p>设年干.</p>
     *
     * @param 年干 a char
     */
    public void 设年干(char 年干) {
        this.年干 = 年干;
    }

    /**
     * <p>取年支.</p>
     *
     * @return a char
     */
    public char 取年支() {
        return 年支;
    }

    /**
     * <p>设年支.</p>
     *
     * @param 年支 a char
     */
    public void 设年支(char 年支) {
        this.年支 = 年支;
    }

    /**
     * <p>取月干.</p>
     *
     * @return a char
     */
    public char 取月干() {
        return 月干;
    }

    /**
     * <p>设月干.</p>
     *
     * @param 月干 a char
     */
    public void 设月干(char 月干) {
        this.月干 = 月干;
    }

    /**
     * <p>取月支.</p>
     *
     * @return a char
     */
    public char 取月支() {
        return 月支;
    }

    /**
     * <p>设月支.</p>
     *
     * @param 月支 a char
     */
    public void 设月支(char 月支) {
        this.月支 = 月支;
    }

    /**
     * <p>取日干.</p>
     *
     * @return a char
     */
    public char 取日干() {
        return 日干;
    }

    /**
     * <p>设日干.</p>
     *
     * @param 日干 a char
     */
    public void 设日干(char 日干) {
        this.日干 = 日干;
    }

    /**
     * <p>取日支.</p>
     *
     * @return a char
     */
    public char 取日支() {
        return 日支;
    }

    /**
     * <p>设日支.</p>
     *
     * @param 日支 a char
     */
    public void 设日支(char 日支) {
        this.日支 = 日支;
    }

    /**
     * <p>取时天干.</p>
     *
     * @return a char
     */
    public char 取时天干() {
        return 时天干;
    }

    /**
     * <p>设时天干.</p>
     *
     * @param 小时天干 a char
     */
    public void 设时天干(char 小时天干) {
        this.时天干 = 小时天干;
    }

    /**
     * <p>取时地支.</p>
     *
     * @return a char
     */
    public char 取时地支() {
        return 时地支;
    }

    /**
     * <p>设时地支.</p>
     *
     * @param 小时地支 a char
     */
    public void 设时地支(char 小时地支) {
        this.时地支 = 小时地支;
    }

    /**
     * <p>取时辰符号.</p>
     *
     * @return a char
     */
    public char 取时辰符号() {
        return 时辰符号;
    }

    /**
     * <p>设时辰符号.</p>
     *
     * @param 时辰符号 a char
     */
    public void 设时辰符号(char 时辰符号) {
        this.时辰符号 = 时辰符号;
    }

    /**
     * <p>取分钟.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String 取分钟() {
        return 分钟;
    }

    /**
     * <p>设分钟.</p>
     *
     * @param 分钟 a {@link java.lang.String} object
     */
    public void 设分钟(String 分钟) {
        this.分钟 = 分钟;
    }

    /**
     * <p>取秒.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String 取秒() {
        return 秒;
    }

    /**
     * <p>设秒.</p>
     *
     * @param 秒 a {@link java.lang.String} object
     */
    public void 设秒(String 秒) {
        this.秒 = 秒;
    }

    /**
     * <p>取模式.</p>
     *
     * @return a {@link gitee.blacol.chineseTime.entity.TransferMode} object
     */
    public TransferMode 取模式() {
        return 模式;
    }

    /**
     * <p>设模式.</p>
     *
     * @param 模式 a {@link gitee.blacol.chineseTime.entity.TransferMode} object
     */
    public void 设模式(TransferMode 模式) {
        this.模式 = 模式;
    }

    /**
     * <p>获取日期.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String 获取日期(){
        return switch (模式){
            case 传统日期时间_截止到时辰->String.format("%c%c年%c%c月%c%c日 %c时",年干,年支,月干,月支,日干,日支, 时地支);
            case 日期-> String.format("%c%c年%c%c月%c%c日",年干,年支,月干,月支,日干,日支);
            case 传统日期时间-> {
                if (时辰符号=='\u0000'){
                    yield String.format("%c%c年%c%c月%c%c日 %c%c时%s分%s秒", 年干, 年支, 月干, 月支, 日干, 日支, 时天干, 时地支, 分钟, 秒);
                }else{
                    yield String.format("%c%c年%c%c月%c%c日 %c%c%c时%s分%s秒", 年干, 年支, 月干, 月支, 日干, 日支, 时天干, 时地支, 时辰符号,分钟, 秒);
                }
            }
            case 完整的日期时间_简记 -> String.format("%c%c年%c%c月%c%c日 %c%c时%s分%s秒",年干,年支,月干,月支,日干,日支,时地支,时辰符号,分钟,秒);
            case 指定格式->{
                StringBuffer stringBuffer=new StringBuffer(String.format("%c%c年%c%c月%c%c日",年干,年支,月干,月支,日干,日支));
                if (时地支 !='\u0000'&&时天干!='\u0000'&&时辰符号!='\u0000'){
                    stringBuffer.append(String.format(" %c%c%c时", 时天干,时地支,时辰符号));
                }else if (时地支 !='\u0000'&&时天干!='\u0000'&&时辰符号=='\u0000'){
                    stringBuffer.append(String.format(" %c%c时", 时天干,时地支));
                }
                if (分钟!=null){
                    stringBuffer.append(String.format("%s分",分钟));
                }
                if (秒!=null){
                    stringBuffer.append(String.format("%s秒",秒));
                }
                yield stringBuffer.toString();
            }
        };
    }
}

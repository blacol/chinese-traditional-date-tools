package gitee.blacol.chineseTime.exception;

public class FormatException extends Exception{
    /**
     * <p>Constructor for FormatException.</p>
     *
     * @param message a {@link java.lang.String} object
     */
    public FormatException(String message) {
        super(message);
    }
}
